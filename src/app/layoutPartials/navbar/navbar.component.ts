import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApplicationUser } from 'src/app/auth/applicationUser.class';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  currentUser: ApplicationUser;
  userSubscription: Subscription = Subscription.EMPTY;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUser;
    this.userSubscription = this.authService.currentUserObserv.subscribe((user: ApplicationUser) => {
      this.currentUser = user;
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  logout() {
    this.authService.logout();
  }

}
