export class ApplicationUser {
    userId: number;
    username: string;
    password: string;
    ownsGame: boolean;
}