import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ApplicationUser } from './applicationUser.class';
import jwt from 'jsonwebtoken';
import {default as decode} from 'jwt-decode'
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private currentUserSubject: BehaviorSubject<any>;
  public currentUserObserv: Subject<ApplicationUser>  = new Subject<ApplicationUser>();
  public currentUser: ApplicationUser;
  public tokenObject;

  constructor(private http: HttpClient, private router: Router) {
    //this.currentUserObserv = new Subject<ApplicationUser>();
    const tokenStorage = localStorage.getItem('currentUser');
    if (tokenStorage) {
      console.log("Logged in User Exists")
      // this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(tokenStorage));
      this.tokenObject = JSON.parse(tokenStorage);
      this.currentUser = decode(this.tokenObject.access_token);

      this.currentUserObserv.next(this.currentUser);
    } else {
      this.currentUser = null;
      this.currentUserObserv.next(this.currentUser);
      // console.log("User not logged in. Redirect to login page.")
      // this.router.navigate(['/login']);
    }
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserObserv.next(null);
    this.currentUser = null;
    this.tokenObject = null;
    this.router.navigate(['/login']);
  }

  login(username: string, password: string) {
    return this.http.post<any>('/api/user/login', { username, password }).toPromise().then(user => {
        // login successful if there's a jwt token in the response
        if (user && user.access_token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUser = decode(user.access_token);
          this.currentUserObserv.next(this.currentUser);
          this.tokenObject = user;
          // this.currentUserSubject.next(user);
          this.router.navigate(['/']);
          return true;
        }

        return false;
      });
  }
}
