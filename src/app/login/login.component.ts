import { Component, OnInit } from '@angular/core';
import { loginUser } from './loginUser.class';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]]
  })

  constructor(private http: HttpClient, private router: Router, private authService: AuthService, private formBuilder: FormBuilder) { }

  ngOnInit() {
  }

  onSubmit(data) {
    this.authService.login(data.username, data.password).then((bool) => {
      if(!bool) {
        alert("Login fehlgeschlagen");
      }
    })
  }
}
