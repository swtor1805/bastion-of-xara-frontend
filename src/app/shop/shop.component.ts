import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  currentUser

  constructor(private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUser;
  }

  buyGame() {
    let data = {userId: this.currentUser.userId};
    this.http.post('/api/user/buygame', data).toPromise().then(response => {
      if (response) {
        this.authService.currentUser.ownsGame = true;
        alert("Kaufen erfolgreich");
      } else {
        alert("Fehler beim kaufen");
      }
    })
  }

}
