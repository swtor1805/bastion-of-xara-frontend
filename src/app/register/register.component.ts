import { Component, OnInit } from '@angular/core';
import { registerUser } from './registerUser.class';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private formBuilder: FormBuilder) { }

  form: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
    passwordTwo: ['', [Validators.required]],
    email: ['', [Validators.required]]
  })

  ngOnInit() {
  }

  onSubmit(data) {
    console.log(data);
    let user: registerUser = new registerUser();

    user.username = data.username;
    user.password = data.password;
    user.passwordTwo = data.passwordTwo;
    user.email = data.email;
    this.http.post('/api/user/register', user).toPromise().then((response: any) => {
      if(response.isValid) {
        alert("Benutzer wurde erfolgreich registriert.");
        this.router.navigate(['/login']);
      } else {
        alert("Fehler beim registrieren: " + response.info);
      }
    })
  }

}
